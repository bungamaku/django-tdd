var acc = document.getElementsByClassName("accordion");
var i, timeOut;

$(document).ready(function() {
    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight){
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = panel.scrollHeight + "px";
        } 
      });
    }
})

function changeColor($elm, classes) {                
    var className = $elm.attr('class');                
    var index = $.inArray(className, classes);

    $elm.removeClass(className);
    index++;                
    if (index === classes.length) {
        index = 0;
    }
    $elm.addClass(classes[index]);                
}        

$(function(){            
    var $button = $("#change-button");
    var body = ["body-light", "body-dark"];
    var footer = ["footer-light", "footer-dark"];
    $button.on("click", function (){                
        changeColor($("body"), body); 
        changeColor($("footer"), footer);    
    });
});

function loading() {
    timeOut = setTimeout(hideLoader, 2000)
}

function hideLoader() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("overlay").style.display = "none";
}