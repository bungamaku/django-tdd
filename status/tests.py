from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from django.utils import timezone
from .models import StatusModel
from .views import status, about
from .forms import StatusForm
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


class UnitTest(TestCase):
    def test_status_url_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)

    def test_status_using_status_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, status)

    def test_status_template_used(self):
        response = Client().get('/status/')
        self.assertTemplateUsed(response, 'status.html')

    def test_status_object_to_database(self):
        StatusModel.objects.create(content='hai', date=timezone.now())
        count_status = StatusModel.objects.all().count()
        self.assertEqual(count_status, 1)

    def test_form_validated(self):
        form = StatusForm(data={'content':'', 'date':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['your_status'],
            ['This field is required.']
        )

    def test_status_page_is_completed(self):
        test_str = 'hai'
        response_post = Client().post('/status/', {'content': test_str, 'date': timezone.now()})
        self.assertEqual(response_post.status_code, 200)
        response = Client().get('/status/')
        html_response = response.content.decode('UTF-8')

    def test_status_deleted_all(self):
        StatusModel.objects.create(content='hai', date=timezone.now())
        response = Client().get('/delete-all/')
        count_status = StatusModel.objects.all().count()
        self.assertEqual(count_status, 0)

    def test_about_url_is_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_about_using_about_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about)

    def test_about_template_used(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about.html')

class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium =  webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest,self).tearDown()

    def test_add_status_selenium(self):
        selenium = self.selenium
        selenium.get("http://127.0.0.1:8000/status/")

        status_form = selenium.find_element_by_name("your_status")
        submit_button = selenium.find_element_by_name("submit_button")
        status_form.send_keys('I am not fine today :(')
        submit_button.submit()

        status_post = selenium.find_elements_by_class_name("status")
        self.assertIn('I am not fine today :(', selenium.page_source)

