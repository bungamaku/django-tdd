from django.shortcuts import render
from django.http import JsonResponse
import json
import requests

def book_table(request):
	return render(request, 'book.html')

def book_json(request):
	url = 'https://www.googleapis.com/books/v1/volumes?q=quilting'
	data = json.loads(requests.get(url).text)
	return JsonResponse(data)
