$(document).ready(function(){
	$(function() {
		$.ajax({
			url: "/book-json/",
			success: function(result) {
			    $.each(result.items, function(idx, item) {
			        $('<tr>').append(
                        $('<td>').text(idx+1),
                        $('<td>').append('<img src=' + item.volumeInfo.imageLinks.thumbnail + "'/>"),
			            $('<td>').text(item.volumeInfo.title),
			            $('<td>').text(item.volumeInfo.authors),
			            $('<td>').text(item.volumeInfo.publisher),
                        $('<td>').text(item.volumeInfo.publishedDate),
                        $('<td>').append('<img onClick="changeStar(this)" id="star" src="https://i.postimg.cc/RZ4BbCvc/star-dark.png" src2="https://i.postimg.cc/t4SMt9JR/star-yellow.png" width="32px"/>'),
			        ).appendTo('#book-data');
			    });
			}
        });
    });
});

function changeStar(e) {
    var temp = e.src;
    if (temp == 'https://i.postimg.cc/RZ4BbCvc/star-dark.png') {
        document.getElementById('counter').innerHTML = inc();
    } else {
        document.getElementById('counter').innerHTML = dec();
    }
    e.src = e.getAttribute('src2');
    e.setAttribute('src2', temp);
}

var counter = 0;

var inc = (function () {
    return function () {counter += 1; return counter;}
})();

var dec = (function () {
    return function () {counter -= 1; return counter;}
})();

function loading() {
    timeOut = setTimeout(hideLoader, 2000)
}

function hideLoader() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("overlay").style.display = "none";
}