from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from django.utils import timezone
from .views import book_table, book_json
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

class UnitTest(TestCase):
    def test_book_url_is_exist(self):
        response = Client().get('/book/')
        self.assertEqual(response.status_code, 200)

    def test_book_using_book_func(self):
        found = resolve('/book/')
        self.assertEqual(found.func, book_table)

    def test_book_template_used(self):
        response = Client().get('/book/')
        self.assertTemplateUsed(response, 'book.html')

class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium =  webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest,self).tearDown()

    def test_book_table_selenium(self):
        selenium = self.selenium
        selenium.get("http://127.0.0.1:8000/book/")
        self.assertIn('book', selenium.page_source)
