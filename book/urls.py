from django.urls import path
from .views import book_json, book_table

urlpatterns = [
    path('book/', book_table, name='book_table'),
    path('book-json/', book_json, name='book_json'),
]