$(document).ready(function(){
	$(function() {
		$.ajax({
			url: "/book-json/",
			success: function(result) {
			    $.each(result.items, function(idx, item) {
			        $('<tr>').append(
                        $('<td>').text(idx+1),
                        $('<td>').append('<img src=' + item.volumeInfo.imageLinks.thumbnail + "'/>"),
			            $('<td>').text(item.volumeInfo.title),
			            $('<td>').text(item.volumeInfo.authors),
			            $('<td>').text(item.volumeInfo.publisher),
                        $('<td>').text(item.volumeInfo.publishedDate),
                        $('<td>').append('<button onClick="changeStar();"><img src="https://i.postimg.cc/RZ4BbCvc/star-dark.png" src2="https://i.postimg.cc/t4SMt9JR/star-yellow.png" width="32px" id="star"/></button>'),
			        ).appendTo('#book-data');
			    });
			}
        });
    });
});

function changeStar() {
    var star = getElementById('star');
    var temp = star.src;
    star.src = star.getAttribute('src2');
    star.setAttribute('src2', temp);
};

function loading() {
    timeOut = setTimeout(hideLoader, 2000)
}

function hideLoader() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("overlay").style.display = "none";
}